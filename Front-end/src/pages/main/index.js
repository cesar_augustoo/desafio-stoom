//Criando um componente - API
import React, {Component} from 'react';

//Importando API com axios
import api from '../../services/api';

export default class Main extends Component {
    //Este componente é usado quando é mostrado algo em tela, no caso algo da API que está sendo consumida
    componentDidMount() {
        this.loadProducts();
    }

    loadProducts = async () => {
        const response = await api.get('/products');

        console.log(response.data.docs);
    };


    render() {
        return <h1>Hello rockeseat</h1>
    }
}