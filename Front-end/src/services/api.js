//Biblioteca para fazer chamadas na api com react 'Axios'
import axios from  'axios';

const api = axios.create({baseURL: 'http://localhost:3001/api'});

//Torna importavel esse componente
export default api;